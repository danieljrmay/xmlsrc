#!/usr/bin/make -f
#
# XMLSrc GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
# See: https://gitlab.com/danieljrmay/xmlsrc
#
# MIT Expat License
# Copyright (c) 2018 Daniel J. R. May
#

# Makefile command variables
DNF_INSTALL=/usr/bin/dnf --assumeyes install
GIT=/usr/bin/git
INSTALL=/usr/bin/install
INSTALL_DATA=$(INSTALL) --mode=644 -D 
INSTALL_DIR=$(INSTALL) --directory
INSTALL_PROGRAM=$(INSTALL) --mode=755 -D
MOCK=/usr/bin/mock
NODE=/usr/bin/node
NPM=/usr/bin/npm
RPMLINT=/usr/bin/rpmlint
SASS=/usr/bin/sassc
SASS_EXPANDED=$(SASS) --style expanded --line-numbers
SASS_COMPRESSED=$(SASS) --style compressed
SHELL=/bin/sh
SHELLCHECK=/usr/bin/shellcheck
XMLLINT=/usr/bin/xmllint --noout
XMLLINT_XHTML=$(XMLLINT) --schema http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd
XMLLINT_XMLSRC=$(XMLLINT) --schema xsd/xmlsrc.xsd
XMLLINT_XSDL=$(XMLLINT) --schema http://www.w3.org/2009/XMLSchema/XMLSchema.xsd
YAMLLINT=/usr/bin/yamllint

# Standard Makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# Fedora installation directory overrides.
#
# We override some of the previous GNU/RPM default values with those
# values suiteable for a Fedora/RedHat/CentOS linux system, as defined
# in /usr/lib/rpm/redhat/macros.
#
# This section can be put into a separate file and included here if we
# want to create a more multi-platform Makefile system.
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib
xmldatadir=$(datadir)/xml

# Fedora mock parameters
mock_root=default
mock_resultdir=.

# Makefile parameter variables
css_files:=css/xmlsrc.css css/xmlsrc.minified.css
version=$(shell awk '/Version:/ { print $$2 }' xmlsrc.spec)
dist_name:=xmlsrc-$(version)
dist_tree-ish:=$(version)

.PHONY: all
all: $(css_files)
	$(info All files have been built.)

css:
	mkdir -p css

css/%.css: scss/%.scss css
	$(SASS_EXPANDED) $< $@

css/%.minified.css: scss/%.scss css
	$(SASS_COMPRESSED) $< $@

xml2xmlsrc/node_modules:
	(cd xml2xmlsrc && $(NPM) install)

.PHONY: lint
lint: xml2xmlsrc/node_modules
	$(info Linting source files:)
	$(YAMLLINT) .gitlab-ci.yml
	$(XMLLINT) examples/complicated.xml
	$(XMLLINT_XMLSRC) examples/*.xmlsrc.xml
	$(XMLLINT_XHTML) examples/simple.xhtml
	$(SHELLCHECK) git-hooks/pre-commit xmlsrclint.bash
	$(XMLLINT_XSDL) xsd/xmlsrc.xsd
	$(RPMLINT) xmlsrc.spec
	(cd xml2xmlsrc && $(NPM) run lint)

.PHONY: installgithooks
installgithooks: .git/hooks/pre-commit
	$(info All git-hook have been installed in .git/hooks directory.)

.git/hooks/pre-commit: git-hooks/pre-commit
	cp $< $@

.PHONY: install
install: all
	$(INSTALL_PROGRAM) xmlsrclint.bash $(DESTDIR)$(bindir)/xmlsrclint
	$(INSTALL_DATA) xsd/xmlsrc.xsd $(DESTDIR)$(xmldatadir)/xmlsrc/xsd

.PHONY: smoketest
smoketest: xml2xmlsrc/node_modules
	$(info Running smoke test:)
	(cd xml2xmlsrc && $(NODE) index.js -h)

.PHONY: test
test: xml2xmlsrc/node_modules
	$(info Running tests:)
	(cd xml2xmlsrc && $(NPM) test)

.PHONY: dist
dist:
	$(GIT) archive --format=tar --prefix=$(dist_name)/ $(dist_tree-ish) | bzip2 > $(dist_name).tar.bz2

.PHONY: srpm
srpm: dist
	$(MOCK) --root=$(mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec xmlsrc.spec --sources $(dist_name).tar.bz2

.PHONY: rpm
rpm: srpm
	$(MOCK) --root=$(mock_root) --resultdir=$(mock_resultdir) --rebuild $(dist_name)*.src.rpm

.PHONY: distclean
distclean:
	rm -rf css xml2xmlsrc/node_modules
	rm -f .git/hooks/pre-commit xmlsrc-*.tar.bz2 xmlsrc-*.rpm *.log *~

.PHONY: requirements
requirements:
	sudo $(DNF_INSTALL) libxml2 make nodejs npm rpmlint sassc ShellCheck yamllint

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all              The default target, builds all files.)
	$(info   lint             Lint all source files.)
	$(info   installgithooks  Install all git hooks in the local repository.)
	$(info   smoketest        Run a smoke test.)
	$(info   test             Run the full test suite.)
	$(info   distclean        Clean up all generated files.)
	$(info   requirements     Install all project requirements, requires sudo.)
	$(info   help             Display this help message.)
	$(info   printvars        Print variable values (useful for debugging).)
	$(info   printmakevars    Print the Make variable values (useful for debugging).)
	$(info )
	$(info For more information read the Makefile and see http://www.gnu.org/software/make/manual)

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info SHELL=$(SHELL))
	$(info SASS=$(SASS))
	$(info SASS_EXPANDED=$(SASS_EXPANDED))
	$(info SASS_COMPRESSED=$(SASS_COMPRESSED))
	$(info XMLLINT=$(XMLLINT))
	$(info XMLLINT_XMLSRC=$(XMLLINT_XMLSRC))
	$(info XMLLINT_XHTML=$(XMLLINT_XHTML))
	$(info XMLLINT_XSDL=$(XMLLINT_XSDL))
	$(info SHELLCHECK=$(SHELLCHECK))
	$(info DNF_INSTALL=$(DNF_INSTALL))
	$(info css_files=$(css_files))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
