<!-- markdownlint-disable MD033 -->
# <img src="doc/images/logo.png" width="100" alt="logo"/> XMLSrc

[![pipeline status](https://gitlab.com/danieljrmay/xmlsrc/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/xmlsrc/commits/master)

*XMLSrc* provides a means of displaying XML source code in a browser.

You can find out more at the [project homepage](https://gitlab.com/danieljrmay/xmlsrc).

## License

*XMLSrc* is available freely under the
[MIT Expat license](https://opensource.org/licenses/MIT).

## Requirements

This software has the following requirements, depending on what you
want to do:

<dl>
<dt>Use</dt>
<dd><a href="https://nodejs.org/en">Node.js</a>.</dd>
<dt>Build</dt>
<dd><a href="https://www.gnu.org/software/make">GNU Make</a>, <a href="https://github.com/sass/sassc">SassC</a>.</dd>
<dt>Develop</dt>
<dd><a href="https://git-scm.com/">Git</a>, <a href="http://xmlsoft.org/">libxml2</a>.</dd>
</dl>

### Node.js

The `xml2xmlsrc` command line tool which converts XML to XMLSrc is
based on [Node.js](https://nodejs.org/en/). You can typically
install *Node.js* on an RPM-based Linux distribution with something like
`dnf install node` or similar.

### GNU Make

A [*GNU Make*](https://www.gnu.org/software/make/) style *Makefile* is
used to co-ordinate the building, linting, testing and installing of
this project's files.

### SassC

[*SassC*](https://github.com/sass/sassc) is the default Sass CSS
pre-processor used in the project Makefile to create the `css/*` files
from the `scss/*` files.

You can typically install *SassC* on an RPM-based Linux distribution
with something like `dnf install sassc` or similar.

You could use a different Sass processor by overriding the `SASS`
Makefile variable with something like `make SASS=my-sass-cmd`. You
might need to override `SASS_EXPANDED` and `SASS_COMPRESSED` as well
depending on whether your processors option switch syntax matches
*SassC*.

### Git

This project’s source code is hosted in a [GitLab
repository](https://gitlab.com/danieljrmay/xmlsrc), so you will want
[Git](https://git-scm.com/) installed if you want to participate in
development.

### XMLLint

The `xmllint` command is used in by the `make lint` Makefile target to
check the validity of the project’s XML source files against their XML
Schemas. The `xmllint` command is provided by the
[libxml2](http://xmlsoft.org/) package. You can typically install this
on an RPM-based Linux distribution with something like `dnf install
libxml2` or similar.

## Project files and directories

<dl>
<dt><code>Makefile</code></dt>
<dd>
This <em>GNU Make</em> style <em>Makefile</em> is used to co-ordinate
the building, linting, testing and installing of this project's files.
</dd>
</dl>
