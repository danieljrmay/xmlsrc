"use strict";

let previewIframe = document.getElementById("preview");
let sourceIframe = document.getElementById("source");

previewIframe.src="source.xhtml";

let xhr = new XMLHttpRequest();
xhr.open("GET", "source.xmlsrc.xml");
xhr.onload = function() {
    console.log("Loaded");
    console.log("Status text=" + this.status);
    console.log("Response text=" + this.responseText);

    
    sourceIframe.src = "data:text/xml;charset=utf-8," + this.responseText;
}
xhr.send(null);

