Name:           xmlsrc
Version:        0.0.4
Release:        1%{?dist}
Summary:        Display XML source code in a browser with syntax highlighting

License:        MIT
URL:            https://gitlab.com/danieljrmay/%{name}
Source0:        https://gitlab.com/danieljrmay/%{name}/-/archive/%{version}/%{name}-%{version}.tar.bz2

BuildArch:      noarch
BuildRequires:  make sassc
Requires:       libxml2


%description
XMLSrc allows you to display XML source code in a browser with syntax
highlighting.


%prep
%setup -q


%build
%make_build


%install
%make_install


%files
%{_bindir}/%{name}lint
%{_datadir}/xml/%{name}
%doc README.md
%license LICENSE

%changelog
* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.0.4-1
- Added logo
- Add make to BuildRequires in spec

* Fri Nov 20 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.0.3-1
- Add draft browser implementation.
- Add elfsrc example.

* Tue Oct 30 2018 Daniel J. R. May <daniel.may@kada-media.com> - 0.0.2-1
- A very minor release to check CI/CD.

* Tue Oct 30 2018 Daniel J. R. May <daniel.may@kada-media.com> - 0.0.1-1
- Initial release.

