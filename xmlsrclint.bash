#!/usr/bin/bash
#
# xmlsrclint.bash
#
# A linter for XMLSrc files.
#
# Version: 0.0.2
# License: MIT Expat License
# Copyright (c) 2018 Daniel J. R. May
#
# See: https://gitlab.com/danieljrmay/xmlsrc

# Debugging switch
readonly DEBUG="true"

# Exit status
#declare -r -i EXIT_OK=0
declare -r -i EXIT_NO_XMLLINT=1
declare -r -i EXIT_NO_SCHEMA=2

# Output a debugging message if in debugging mode
function debug {
    if [[ "$DEBUG" = "true" ]]
    then
	echo "DEBUG: $1"
    fi
}

# Output an error message
function error {
    echo "ERROR: $1"
}

# Output usage
function usage {
    echo "Usage: xmlsrclint [OPTION] [file] ..."
}

# Start
debug "Running xmlsrclint in debugging mode."

# Check we have the xmllint command
if (xmllint --version > /dev/null 2>&1 )
then
    debug "The xmllint command is available."
else
    error "The xmllint command is not installed, on this scripts PATH or not working correctly."
    exit $EXIT_NO_XMLLINT
fi

# Find the XMLSrc schema file xmlsrc.xsd
if [[ -r ./xsd/xmlsrc.xsd ]]
then
    xsd=$(realpath ./xsd/xmlsrc.xsd)
    debug "Using the XMLSrc schema at $xsd"
elif [[ -r /usr/share/xml/xmlsrc/xsd/xmlsrc.xsd ]]
then
    xsd=$(realpath /usr/share/xml/xmlsrc/xsd/xmlsrc.xsd)
    debug "Using the XMLSrc schema at $xsd"
else
    error "Can not find the XMLSrc schema file xmlsrc.xsd"
    exit $EXIT_NO_SCHEMA
fi

# Run the xmllint command
debug "About to run: xmllint --noout --schema $xsd $*"
xmllint --noout --schema "$xsd" "$@"
