#!/usr/bin/env node

"use strict";

const COMMANDER = require("commander");
const FS = require("fs");

const XML2XMLSRC = require("./xml2xmlsrc");

const TRANSLATOR = XML2XMLSRC.getTranslator();
const LOGGER = XML2XMLSRC.getLogger();

const EXIT_STATUS_OK = 0;
const EXIT_STATUS_INVALID_INPUT_PATH = 1;
const EXIT_STATUS_ERROR_WRITING_FILE = 2;


function get_input_xml(path) {
    if (path) {
        LOGGER.debug("input path=" + path);
    } else {
        LOGGER.error("No input file has been specified.");
        process.exit(EXIT_STATUS_INVALID_INPUT_PATH);
    }

    try {
        return FS.readFileSync(path, "utf8");
    } catch(err) {
        LOGGER.error("Unable to read input file contents.");
        process.exit(EXIT_STATUS_INVALID_INPUT_PATH);
    }
}

function write_output_file(path, xmlsrc) {
    LOGGER.debug("output path=" + path);

    try {
        return FS.writeFileSync(path, xmlsrc);
    } catch(err) {
        LOGGER.error("Unable to write output file " + path);
        process.exit(EXIT_STATUS_ERROR_WRITING_FILE);
    }
}

function dispatch(input, options) {
    const XML = get_input_xml(input);
    const XMLSRC = TRANSLATOR.translate(XML);

    if (options.output) {
        write_output_file(options.output, XMLSRC);
    } else {
        console.log(XMLSRC);
    }
}

COMMANDER
    .version("0.6.1", "-v, --version")
    .usage("[options] <file>")
    .option("-o, --output [output]", "output XMLSrc file path")
    .action(dispatch)
    .parse(process.argv);

process.exit(EXIT_STATUS_OK);
