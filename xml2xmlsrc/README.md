# xml2xmlsrc Read Me #

## Executing ##
You can execute the xml2xmlsrc Node.js script with
```bash
node index.js input.xml
```

This will output the xmlsrc to console. You will probably want to output to a file with
```shell
node index.js -o output.xmlsrc.xml input.xml
```

You can get help with the various usage options with
```shell
node index.js -h
```

## Testing ##
You can the test suite with
```shell
npm test
```
