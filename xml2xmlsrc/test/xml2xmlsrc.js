"use strict";

const assert = require("chai").assert;
const xml2xmlsrc = require("../xml2xmlsrc");

/* Change the xml2xmlsrc logger level to "error" so that we do not
 * confuse the test output with debugging messages. */
xml2xmlsrc.getLogger().level = "error";

describe("Translator class", function() {

    it("empty string input", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = "";
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() + INPUT + TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("custom stylesheet, empty string input", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();
        TRANSLATOR.setStylesheet("my/new/stylesheet.css");

        const INPUT = "";
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() + INPUT + TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version only", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml version="1.0"?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version only with spaces", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml  version   =    "1.0"  ?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version only with illegal value for version", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml  version   =    "1.1"  ?>';

        assert.throws(() => TRANSLATOR.translate(INPUT));
    });

    it("xml declaration version and encoding", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml version="1.0" encoding="utf-8"?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version><encoding>utf-8</encoding></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version and encoding with spaces", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml  version   =    "1.0"    encoding    =  "utf-8"?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version><encoding>utf-8</encoding></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version and encoding with alternative encoding value", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml version="1.0" encoding="iso8859-1"?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version><encoding>iso8859-1</encoding></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version, encoding and standalone", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version><encoding>utf-8</encoding><standalone>yes</standalone></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version and encoding with spaces", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml  version   =    "1.0"    encoding    =  "utf-8"   standalone="yes"    ?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version><encoding>utf-8</encoding><standalone>yes</standalone></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version and standalone", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml version="1.0" standalone="no"?>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<xmlDeclaration><version>1.0</version><standalone>no</standalone></xmlDeclaration>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("xml declaration version and standalone with illegal value for standalone", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<?xml version="1.0" standalone="illegal"?>';

        assert.throws(() => TRANSLATOR.translate(INPUT));
    });

    it("empty element", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><name>myelement</name></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("empty element with one attribute", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement myattrname="myattrvalue"/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><name>myelement</name><attribute><name>myattrname</name><value>myattrvalue</value></attribute></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("empty element with two attributes", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement myattrname="myattrvalue" matwo="matwovalue"/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><name>myelement</name><attribute><name>myattrname</name><value>myattrvalue</value></attribute><attribute><name>matwo</name><value>matwovalue</value></attribute></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("empty element with two attributes with extra whitespace", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement          myattrname="myattrvalue"          matwo="matwovalue"   ' + "\r\n\t" + '/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><name>myelement</name><attribute><name>myattrname</name><value>myattrvalue</value></attribute><attribute><name>matwo</name><value>matwovalue</value></attribute></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("prefixed empty element", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<pfx:myelement/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><prefix>pfx</prefix><name>myelement</name></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("prefixed empty element with attribute", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<pfx:myelement a="v"/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><prefix>pfx</prefix><name>myelement</name><attribute><name>a</name><value>v</value></attribute></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("prefixed empty element with prefixed attribute", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<pfx:myelement p:a="v"/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><prefix>pfx</prefix><name>myelement</name><attribute><prefix>p</prefix><name>a</name><value>v</value></attribute></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("prefixed empty element with multiple prefixed attributes", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<pfx:myelement p:a="v" q:bbbb="ccccc"/>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><emptyTag><prefix>pfx</prefix><name>myelement</name><attribute><prefix>p</prefix><name>a</name><value>v</value></attribute><attribute><prefix>q</prefix><name>bbbb</name><value>ccccc</value></attribute></emptyTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement></myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>myelement</name></startTag><endTag><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element with attribute", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement a="b"></myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>myelement</name><attribute><name>a</name><value>b</value></attribute></startTag><endTag><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element with attributes", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement a="b" c="d"></myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>myelement</name><attribute><name>a</name><value>b</value></attribute><attribute><name>c</name><value>d</value></attribute></startTag><endTag><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element with prefixed and non-prefixed attributes", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement p:a="b" c="d"></myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>myelement</name><attribute><prefix>p</prefix><name>a</name><value>b</value></attribute><attribute><name>c</name><value>d</value></attribute></startTag><endTag><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element with prefixed attributes", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<myelement p:a="b" q:c="d"></myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>myelement</name><attribute><prefix>p</prefix><name>a</name><value>b</value></attribute><attribute><prefix>q</prefix><name>c</name><value>d</value></attribute></startTag><endTag><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("prefixed element with prefixed attributes", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<z:myelement p:a="b" q:c="d"></z:myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><prefix>z</prefix><name>myelement</name><attribute><prefix>p</prefix><name>a</name><value>b</value></attribute><attribute><prefix>q</prefix><name>c</name><value>d</value></attribute></startTag><endTag><prefix>z</prefix><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("minimal nested elements", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<p><c/></p>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>p</name></startTag><element><emptyTag><name>c</name></emptyTag></element><endTag><name>p</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("nested elements", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<p><b><c/></b></p>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>p</name></startTag><element><startTag><name>b</name></startTag><element><emptyTag><name>c</name></emptyTag></element><endTag><name>b</name></endTag></element><endTag><name>p</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("nested prefixed elements", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<z:myelement p:a="b" q:c="d"><x:n/></z:myelement>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><prefix>z</prefix><name>myelement</name><attribute><prefix>p</prefix><name>a</name><value>b</value></attribute><attribute><prefix>q</prefix><name>c</name><value>d</value></attribute></startTag><element><emptyTag><prefix>x</prefix><name>n</name></emptyTag></element><endTag><prefix>z</prefix><name>myelement</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("comment", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<!--My comment text-->';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<comment>My comment text</comment>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("comment in element content", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e><!--My comment text--></e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><comment>My comment text</comment><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("cdata", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e><![CDATA[My cdata text]]></e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><cdata>My cdata text</cdata><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("text", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e>My text content.</e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><text>My text content.</text><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("entity", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e>&amp;</e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><text><entity title="&amp;">amp</entity></text><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("text with entity", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e>This &amp; that.</e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><text>This <entity title="&amp;">amp</entity> that.</text><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("text with multiple entities", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e>This &amp; that&apos;s it.</e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><text>This <entity title="&amp;">amp</entity> that<entity title="&apos;">apos</entity>s it.</text><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("text with entities as fist and last character", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<e>&quot;Quoted text.&quot;</e>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>e</name></startTag><text><entity title="&quot;">quot</entity>Quoted text.<entity title="&quot;">quot</entity></text><endTag><name>e</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element with default namespace", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<math xmlns="http://www.w3.org/1998/Math/MathML"><mi>x</mi></math>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>math</name><defaultNamespace><uri>http://www.w3.org/1998/Math/MathML</uri></defaultNamespace></startTag><element><startTag><name>mi</name></startTag><text>x</text><endTag><name>mi</name></endTag></element><endTag><name>math</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("element with default and foreign namespace", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<math xmlns="http://www.w3.org/1998/Math/MathML" xmlns:s="http://www.w3.org/2000/svg"><mi>x</mi></math>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<element><startTag><name>math</name><defaultNamespace><uri>http://www.w3.org/1998/Math/MathML</uri></defaultNamespace><namespace><prefix>s</prefix><uri>http://www.w3.org/2000/svg</uri></namespace></startTag><element><startTag><name>mi</name></startTag><text>x</text><endTag><name>mi</name></endTag></element><endTag><name>math</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("Minimal xhtml5", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head><title>T</title></head><body>T</body></html>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<documentTypeDeclaration><name>html</name></documentTypeDeclaration><element><startTag><name>html</name><defaultNamespace><uri>http://www.w3.org/1999/xhtml</uri></defaultNamespace></startTag><element><startTag><name>head</name></startTag><element><startTag><name>title</name></startTag><text>T</text><endTag><name>title</name></endTag></element><endTag><name>head</name></endTag></element><element><startTag><name>body</name></startTag><text>T</text><endTag><name>body</name></endTag></element><endTag><name>html</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("Minimal xhtml1.0strict", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>T</title></head><body>T</body></html>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<documentTypeDeclaration><name>html</name><publicExternalEntity><pubIdLiteral>-//W3C//DTD XHTML 1.0 Strict//EN</pubIdLiteral><systemLiteral>http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd</systemLiteral></publicExternalEntity></documentTypeDeclaration><element><startTag><name>html</name><defaultNamespace><uri>http://www.w3.org/1999/xhtml</uri></defaultNamespace></startTag><element><startTag><name>head</name></startTag><element><startTag><name>title</name></startTag><text>T</text><endTag><name>title</name></endTag></element><endTag><name>head</name></endTag></element><element><startTag><name>body</name></startTag><text>T</text><endTag><name>body</name></endTag></element><endTag><name>html</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

    it("Minimal xhtml1.0strict alternative whitespace", function() {
        const TRANSLATOR = xml2xmlsrc.getTranslator();

        const INPUT = '<!DOCTYPE     html     PUBLIC     "-//W3C//DTD XHTML 1.0 Strict//EN"       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"     ><html xmlns="http://www.w3.org/1999/xhtml"><head><title>T</title></head><body>T</body></html>';
        const EXPECTED_OUTPUT = TRANSLATOR._getHeader() +
              '<documentTypeDeclaration><name>html</name><publicExternalEntity><pubIdLiteral>-//W3C//DTD XHTML 1.0 Strict//EN</pubIdLiteral><systemLiteral>http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd</systemLiteral></publicExternalEntity></documentTypeDeclaration><element><startTag><name>html</name><defaultNamespace><uri>http://www.w3.org/1999/xhtml</uri></defaultNamespace></startTag><element><startTag><name>head</name></startTag><element><startTag><name>title</name></startTag><text>T</text><endTag><name>title</name></endTag></element><endTag><name>head</name></endTag></element><element><startTag><name>body</name></startTag><text>T</text><endTag><name>body</name></endTag></element><endTag><name>html</name></endTag></element>' +
              TRANSLATOR._getFooter();
        const OUTPUT = TRANSLATOR.translate(INPUT);

        assert.strictEqual(OUTPUT, EXPECTED_OUTPUT);
    });

});
