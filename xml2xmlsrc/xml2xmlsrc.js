/**
 * @fileOverview Translator class which converts XML to XMLSRC.
 *
 * @author Daniel J. R. May
 *
 * @requires NPM:winston
 */

"use strict";

/**
 * Configure winston based logging.
 */
const WINSTON = require("winston");
const LOGGER = WINSTON.createLogger({
    level: "silly",
    format: WINSTON.format.simple(),
    transports: [ new WINSTON.transports.Console({})]
});

/**
 * The Translator class uses a finite state machine model to parse the
 * input XML. This is the list of different states.
 */
const MODE_START = 0;
const MODE_XML_DECLARATION_OR_PROCESSING_INSTRUCTION = 1;
const MODE_ELEMENT_START_OR_EMPTY_TAG = 3;
const MODE_XML_DECLARATION = 4;
const MODE_XML_DECLARATION_EQUALS = 5;
const MODE_XML_DECLARATION_VERSION = 6;
const MODE_XML_DECLARATION_ENCODING = 7;
const MODE_XML_DECLARATION_STANDALONE = 8;
const MODE_PROCESSING_INSTRUCTION_DATA = 9;
const MODE_COMMENT = 10;
const MODE_CDATA = 11;
const MODE_DOCTYPE = 12;
const MODE_DOCTYPE_NAME = 21;
const MODE_ELEMENT_CONTENT = 13;
const MODE_ELEMENT_END_TAG = 14;
const MODE_ATTRIBUTE = 15;
const MODE_ATTRIBUTE_PREFIX_OR_NAME = 16;
const MODE_ATTRIBUTE_EQUALS = 17;
const MODE_ATTRIBUTE_VALUE = 18;
const MODE_REFERENCE = 19;
const MODE_TEXT = 20;
const MODE_CHARACTER_REFERENCE = 22;
const MODE_DOCTYPE_CONTENT = 23;
const MODE_DOCTYPE_SYSTEM_LITERAL = 24;
const MODE_DOCTYPE_PUBLIC_LITERAL = 25;
const MODE_DOCTYPE_INTERNAL_SUBSET = 26;

/**
 * These constants are used to keep track of whether an attribute
 * value in the input XML is delimited with single or double
 * quotes. The undefined value is used to show an undefined state
 * before the attribute value delimiter has been detected.
 */
const ATTRIBUTE_VALUE_QUOTES_UNDEFINED = 0;
const ATTRIBUTE_VALUE_QUOTES_SINGLE = 1;
const ATTRIBUTE_VALUE_QUOTES_DOUBLE = 2;


/**
 * Translates XML to XMLSRC.
 */
class Translator {
    /**
     * Create a translator.
     *
     * @param {String} stylesheet Path to a custom XMLSRC CSS stylesheet.
     */
    constructor(stylesheet = "../css/xmlsrc.css") {
        this.stylesheet = stylesheet;
    }

    /**
     * Set a new XMLSRC CSS stylesheet.
     *
     * @param {String} stylesheet Path to a custom XMLSRC CSS stylesheet.
     */
    setStylesheet(stylesheet) {
        this.stylesheet = stylesheet;
    }

    /**
     * Translate the input XML to XMLSRC.
     *
     * @param {String} xml The input XML.
     * @return {String} The output XMLSRC.
     */
    translate(xml) {
        /* Initialize all parameters. */
        this.xml = xml;
        this._init();
        const LENGTH = xml.length;

        /* Scan the input XML dispatching to different methods
         * depending on the current mode we are in. */
        while (this.index < LENGTH) {
            switch (this._mode) {
            case MODE_START:
                this._modeStart();
                break;

            case MODE_XML_DECLARATION_OR_PROCESSING_INSTRUCTION:
                this._modeXmlDeclarationOrProcessingInsturction();
                break;

            case MODE_XML_DECLARATION:
                this._modeXmlDeclaration();
                break;

            case MODE_XML_DECLARATION_EQUALS:
                this._modeXmlDeclarationEquals();
                break;

            case MODE_XML_DECLARATION_VERSION:
                this._modeXmlDeclarationVersion();
                break;

            case MODE_XML_DECLARATION_ENCODING:
                this._modeXmlDeclarationEncoding();
                break;

            case MODE_XML_DECLARATION_STANDALONE:
                this._modeXmlDeclarationStandalone();
                break;

            case MODE_COMMENT:
                this._modeComment();
                break;

            case MODE_CDATA:
                this._modeCdata();
                break;

            case MODE_PROCESSING_INSTRUCTION_DATA:
                this._modeProcessingInstructionData();
                break;

            case MODE_DOCTYPE:
                this._modeDoctype();
                break;

            case MODE_DOCTYPE_NAME:
                this._modeDoctypeName();
                break;

            case MODE_DOCTYPE_CONTENT:
                this._modeDoctypeContent();
                break;

            case MODE_DOCTYPE_SYSTEM_LITERAL:
                this._modeDoctypeSystemLiteral();
                break;

            case MODE_DOCTYPE_PUBLIC_LITERAL:
                this._modeDoctypePublicLiteral();
                break;

            case MODE_ELEMENT_START_OR_EMPTY_TAG:
                this._modeElementStartOrEmptyTag();
                break;

            case MODE_ELEMENT_CONTENT:
                this._modeElementContent();
                break;

            case MODE_ELEMENT_END_TAG:
                this._modeElementEndTag();
                break;

            case MODE_ATTRIBUTE:
                this._modeAttribute();
                break;

            case MODE_ATTRIBUTE_PREFIX_OR_NAME:
                this._modeAttributePrefixOrName();
                break;

            case MODE_ATTRIBUTE_EQUALS:
                this._modeAttributeEquals();
                break;

            case MODE_ATTRIBUTE_VALUE:
                this._modeAttributeValue();
                break;

            case MODE_REFERENCE:
                this._modeReference();
                break;

            case MODE_TEXT:
                this._modeText();
                break;

            case MODE_CHARACTER_REFERENCE:
                this._modeCharacterReference();
                break;

            default:
                throw "Unknown translater mode " + this._mode;
            }

            /* Increment the index of the input XML string and update
             * the line and character line counters if we detect a new
             * line character. */
            this.index++;
            this.currentChar = this.xml.charAt(this.index);

            if (this._isNewLineChar(this.index)) {
                this.lineNumber++;
                this.lineCharacter = 0;
            } else {
                this.lineCharacter++;
            }
        }

        /* Concatenate and return the the XMLSRC header, body and
         * footer strings. */
        return this._getHeader() + this.body + this._getFooter();
    }

    /**
     * Getter to obtain the current mode.
     *
     * @return {number} The mode value at the top of the mode stack.
     */
    get _mode() {
        return this.modeStack[this.modeStack.length - 1];
    }

    /**
     * Getter to obtain the current token.
     *
     * @return {String} Get the current token.
     */
    get _token() {
        return this.xml.slice(this.tokenStart, this.tokenEnd + 1);
    }

    /**
     * Initialize all member fields to their default values.
     */
    _init() {
        this.modeStack = [];
        this.modeStack.push(MODE_START);
        this.index = 0;
        this.lineNumber = 1;
        this.lineCharacter = 1;
        this.body = "";
        this.tokenStart = 0;
        this.tokenEnd = 0;
        this.currentChar = this.xml.charAt(this.index);
        this.xmlDeclarationVersionSet = false;
        this.doctypeNameSet = false;
        this.hexidecimalCharacterReferenceSet = false;
        this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_UNDEFINED;
        this.publicExternalEntity = false;

        this._resetAttributeCache();
        this._resetElementCache();

        LOGGER.silly(this._getMessage("Initialized."));
    }

    /**
     * Reset all attribute related variables to their default values.
     */
    _resetAttributeCache() {
        LOGGER.silly(this._getMessage("Reset attribute cache."));
        this.attributePrefix = null;
        this.attributeName = null;
        this.attributeValue = null;
        this.attributeValueQuotes = ATTRIBUTE_VALUE_QUOTES_UNDEFINED;
    }

    /**
     * Append XMLSrc to the elementAttributes memeber according to the
     * recorded values of the attributePrefix, attributeName and
     * atttributeValue members.
     */
    _updateElementAttributes() {
        LOGGER.silly(this._getMessage("Updating element attributes."));

        if (this.attributePrefix && this.attributePrefix === "xmlns") {
            this.elementAttributes += "<namespace><prefix>" + this.attributeName +
                "</prefix><uri>" + this.attributeValue + "</uri></namespace>";
        } else if (this.attributeName === "xmlns") {
            this.elementAttributes += "<defaultNamespace><uri>" + this.attributeValue + "</uri></defaultNamespace>";
        } else {
            this.elementAttributes += this.attributePrefix ? "<attribute><prefix>" + this.attributePrefix + "</prefix>" : "<attribute>";
            this.elementAttributes += "<name>" + this.attributeName + "</name><value";
            this.elementAttributes += (this.attributeValueQuotes === ATTRIBUTE_VALUE_QUOTES_SINGLE) ? ' singleQuotes="true"' : "";
            this.elementAttributes += ">" + this.attributeValue + "</value></attribute>";
        }

        this._resetAttributeCache();
    }

    /**
     * Reset the element cache mumbers: elementPrefix, elementName and
     * elementAttribute to their null values. This should be called
     * whenever we have detected a new element.
     */
    _resetElementCache() {
        LOGGER.silly(this._getMessage("Reset attribute cache."));
        this.elementPrefix = null;
        this.elementName = null;
        this.elementAttributes = "";
    }

    /**
     * Get the character at the specified index from the input XML
     * String.
     *
     * @param {number} i The index of the character to return.
     * @return {String} A one character long string representing the
     * specified character of the input XML string.
     */
    _getChar(i) {
        return this.xml.charAt(i);
    }

    /**
     * Check if the character at the specified index of the input XML
     * string is a legal XML character.
     *
     * @param {number} i The index of the character to check.
     * @return {boolean} True if the specified character is a legal
     * XML character, false otherwise.
     * @see https://www.w3.org/TR/xml/#charsets
     */
    _isChar(i) {
        const C = this.xml.charCodeAt(i);

        if (
            C === 0x9 ||  /* Horizontal tab */
            C === 0xA ||  /* Line feed */
            C === 0xD ||  /* Carriage return */
            (C >= 0x20    && C <= 0xD7FF) ||
            (C >= 0xE000  && C <= 0xFFFD) ||
            (C >= 0x10000 && C <= 0x10FFFF)
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the character at the specified index of the input XML
     * string is a new line character.
     *
     * @param {number} i The index of the character to check.
     * @return {boolean} True if the specified character is a line
     * feed or carriage return character, false otherwise.
     */
    _isNewLineChar(i) {
        switch(this.xml.charCodeAt(i)) {
        case 0xA: /* Line feed */
        case 0xD: /* Carriage return */
            return true;

        default:
            return false;
        }
    }

    /**
     * Check if the character at the specified index of the input XML
     * string is a white space character.
     *
     * @param {number} i The index of the character to check.
     * @return {boolean} True if the specified character is a white
     * space character, false otherwise.
     * @link https://www.w3.org/TR/xml/#NT-S
     */
    _isWhiteSpaceChar(i) {
        switch (this.xml.charCodeAt(i)) {
        case 0x9:  /* Horizontal tab */
        case 0xA:  /* Line feed */
        case 0xD:  /* Carriage return */
        case 0x20: /* Space */
            return true;

        default:
            return false;
        }
    }

    /**
     * Check if the character at the specified index of the input XML
     * string is a name start character.
     *
     * @param {number} i The index of the character to check.
     * @return {boolean} True if the specified character is a white
     * space character, false otherwise.
     * @link https://www.w3.org/TR/xml/#NT-NameStartChar
     */
    _isNameStartChar(i) {
        const C = this.xml.charCodeAt(i);

        if ((C >= 0x41 && C <= 0x5A) || /* [A-Z] */
            (C >= 0x61 && C <= 0x7A) || /* [a-z] */
            (C === 0x3A) ||  /* [:] */
            (C === 0x5F) ||  /* [_] */
            (C >= 0xC0 && C <= 0xD6)     || /* [#xC0-#xD6] */
            (C >= 0xF8 && C <= 0x2FF)    || /* [#xF8-#x2FF] */
            (C >= 0x370 && C <= 0x37D)   || /* [#x370-#x37D] */
            (C >= 0x37F && C <= 0x1FFF)  || /* [#x37F-#x1FFF] */
            (C >= 0x200C && C <= 0x200D) || /* [#x200C-#x200D] */
            (C >= 0x2070 && C <= 0x218F) || /* [#x2070-#x218F] */
            (C >= 0x2C00 && C <= 0x2FEF) || /* [#x2C00-#x2FEF] */
            (C >= 0x3001 && C <= 0xD7FF) || /* [#x3001-#xD7FF] */
            (C >= 0xF900 && C <= 0xFDCF) || /* [#xF900-#xFDCF] */
            (C >= 0xFDF0 && C <= 0xFFFD) || /* [#xFDF0-#xFFFD] */
            (C >= 0x10000 && C <= 0xEFFFF)  /* [#x10000-#xEFFFF] */
           ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the character at the specified index of the input XML
     * string is a name character.
     *
     * @param {number} i The index of the character to check.
     * @return {boolean} True if the specified character is a name
     * character, false otherwise.
     * @link https://www.w3.org/TR/xml/#NT-NameChar
     */
    _isNameChar(i) {
        if (this._isNameStartChar(i)) {
            return true;
        }

        switch (this.xml.charAt(this.i)) {
        case "-":
        case ".":
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
            return true;
        }

        const C = this.xml.charCodeAt(i);

        if (
            (C === 0xB7) ||  /* [·] */
            (C >= 0x0300 && C <= 0x036F) ||
            (C >= 0x203F && C <= 0x2040)
           ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Format a logging message or error string.
     *
     * @param {String} message The message.
     * @param {boolean} displayState Show the current state of the parser.
     * @return {String} A string suitable for logging or throwing.
     */
    _getMessage(message, displayState = true) {
        if (displayState) {
            return "xml[" + this.index + "; " + this.lineNumber + ":" + this.lineCharacter + "]=" +
                this.currentChar + " m=" + this._mode + " " + message;
        } else {
            return message;
        }
    }

    /**
     * Get the XMLSRC header.
     *
     * @return {String} The output XMLSRC header.
     */
    _getHeader() {
        return '<?xml version="1.0" encoding="utf-8"?>' +
            '<?xml-stylesheet type="text/css" href="' + this.stylesheet + '"?>' +
            '<xmlsrc xmlns="https://gitlab.com/danieljrmay/xmlsrc">';
    }

    /**
     * Get the XMLSRC footer.
     *
     * @return {String} The output XMLSRC footer.
     */
    _getFooter() {
        return "</xmlsrc>";
    }

    /**
     * MODE_START: The inital starting mode. We expect one of the following:
     * - XML Declaration
     * - Processing Instruction
     * - Doctype declaration
     * - Comment
     * - Element
     * - White space
     *
     * @link https://www.w3.org/TR/xml/#NT-document
     */
    _modeStart() {
        if (this.currentChar === "<") {
            LOGGER.silly(this._getMessage("Detected '<' character."));

            if (this._getChar(this.index + 1) === "?") {
                LOGGER.silly(this._getMessage("Detected start of an XML Declaration or processing instruction."));
                this.modeStack.push(MODE_XML_DECLARATION_OR_PROCESSING_INSTRUCTION);
                this.index++;
                this.tokenStart = this.index + 1;
            } else if (this.xml.slice(this.index + 1, this.index + 4) === "!--") {
                LOGGER.silly(this._getMessage("Detected start of a comment."));
                this.modeStack.push(MODE_COMMENT);
                this.index += 4;
                this.tokenStart = this.index;
            } else if (this.xml.slice(this.index + 1, this.index + 9) === "!DOCTYPE") {
                LOGGER.silly(this._getMessage("Detected start of a doctype declaration."));
                this.modeStack.push(MODE_DOCTYPE);
                this.index += 9;
                this.body += "<documentTypeDeclaration>";
            } else if (this._isNameStartChar(this.index + 1)) {
                LOGGER.silly(this._getMessage("Detected start of an element."));
                this.modeStack.push(MODE_ELEMENT_START_OR_EMPTY_TAG);
                this.tokenStart = this.index + 1;
            } else {
                throw this._getMessage("Illegal character following '<'.");
            }
        } else if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space character."));
        } else {
            throw this._getMessage("Illegal character.");
        }
    }

    /**
     * MODE_XML_DECLARATION_OR_PROCESSING_INSTRUCTION: We have
     * detected '<?' and are now scanning up to the next whitespace
     * character to see if the next token is "xml" in which case we
     * are dealing with a XML declaration, otherwise we have a
     * processing instruction.
     *
     * @link https://www.w3.org/TR/xml/#NT-XMLDecl
     * @link https://www.w3.org/TR/xml/#NT-PI
     */
    _modeXmlDeclarationOrProcessingInsturction() {
        if (this._isWhiteSpaceChar(this.index)) {
            this.tokenEnd = this.index - 1;

            if (this._token === "xml") {
                LOGGER.silly(this._getMessage("Detected start of XML declaration."));
                this.modeStack.pop();
                this.modeStack.push(MODE_XML_DECLARATION);
                this.body += "<xmlDeclaration>";
            } else {
                LOGGER.silly(this._getMessage("Detected start of processing instruction."));
                this.modeStack.pop();
                this.modeStack.push(MODE_PROCESSING_INSTRUCTION_DATA);
                this.body += "<processingInstruction><target>" + this._token + "</target>";
                this.tokenStart = this.index;
            }
        } else {
            LOGGER.silly(this._getMessage("Assuming an XML declaration or processing instruction target character."));
        }
    }

    /**
     * MODE_XML_DECLARATION: We have detected '<?xml ' and so we are
     * now scanning up to a closing '?>' while looking for the
     * version, encoding and standalone "attributes".
     *
     * @link https://www.w3.org/TR/xml/#NT-XMLDecl
     */
    _modeXmlDeclaration() {
        switch(this.currentChar) {
        case "v":
            if (this.xml.slice(this.index, this.index + 7) === "version") {
                LOGGER.silly(this._getMessage("Detected start of XML declaration version."));
                this.modeStack.push(MODE_XML_DECLARATION_VERSION);
                this.modeStack.push(MODE_XML_DECLARATION_EQUALS);
                this.index += 6;
            } else {
                throw this._getMessage("Illegal character in XML declaration.");
            }
            break;

        case "e":
            if (this.xml.slice(this.index, this.index + 8) === "encoding") {
                LOGGER.silly(this._getMessage("Detected start of XML declaration encoding."));
                this.modeStack.push(MODE_XML_DECLARATION_ENCODING);
                this.modeStack.push(MODE_XML_DECLARATION_EQUALS);
                this.index += 7;
            } else {
                throw this._getMessage("Illegal character in XML declaration.");
            }
            break;

        case "s":
            if (this.xml.slice(this.index, this.index + 10) === "standalone") {
                LOGGER.silly(this._getMessage("Detected start of XML declaration standalone."));
                this.modeStack.push(MODE_XML_DECLARATION_STANDALONE);
                this.modeStack.push(MODE_XML_DECLARATION_EQUALS);
                this.index += 9;
            } else {
                throw this._getMessage("Illegal character in XML declaration.");
            }
            break;

        case "?":
            if (this._getChar(this.index + 1) === ">") {
                LOGGER.silly(this._getMessage("Detected end of XML declaration."));

                if (!this.xmlDeclarationVersionSet) {
                    throw this._getMessage("XML Declaration must specify a version.");
                }

                this.index++;
                this.modeStack.pop();
                this.body += "</xmlDeclaration>";
            }
            break;

        default:
            if (this._isWhiteSpaceChar(this.index)) {
                return;
            } else {
                throw this._getMessage("Illegal XML format in XML Declaration");
            }
        }
    }

    /**
     * MODE_XML_DECLARATION_EQUALS: We just detected an xml
     * declaration attribute and are now munching white space while
     * looking for the '=' character.
     *
     * @link https://www.w3.org/TR/xml/#NT-XMLDecl
     */
    _modeXmlDeclarationEquals() {
        if (this.currentChar === "=") {
            LOGGER.silly(this._getMessage("Detected XML declaration '=' character."));
            this.modeStack.pop();
        } else if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space after XML declaration attribute and before '=' character."));
        } else {
            throw this._getMessage("Illegal character while looking for '=' character in XML Declaration");
        }
    }

    /**
     * MODE_XML_DECLARATION_VERSION: We just detected an xml
     * declaration "version=" attribute and are now looking for the
     * value attribute in quotes.
     *
     * @link https://www.w3.org/TR/xml/#NT-XMLDecl
     */
    _modeXmlDeclarationVersion() {
        if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space after 'version=' in XML declaration."));
            return;
        }

        if (this.xml.slice(this.index, this.index + 5) === "'1.0'") {
            LOGGER.silly(this._getMessage("Detected legal XML declaration version value in single quotes."));
            this.index += 4;
            this.modeStack.pop();
            this.xmlDeclarationVersionSet = true;
            this.body += '<version singleQuotes="true">1.0</version>';
        } else if (this.xml.slice(this.index, this.index + 5) === '"1.0"') {
            LOGGER.silly(this._getMessage("Detected legal XML declaration version value in double quotes."));
            this.index += 4;
            this.modeStack.pop();
            this.xmlDeclarationVersionSet = true;
            this.body += '<version>1.0</version>';
        } else {
            throw this._getMessage("Illegal XML declaration version value.");
        }
    }

    /**
     * MODE_XML_DECLARATION_ENCODING: We just detected an xml
     * declaration "encoding=" attribute and are now looking for the
     * value attribute in quotes.
     *
     * @link https://www.w3.org/TR/xml/#NT-XMLDecl
     */
    _modeXmlDeclarationEncoding() {
        if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space after 'encoding=' in XML declaration."));
            return;
        }

        switch (this.currentChar) {
        case "'":
            this.modeStack.pop();
            this.tokenStart = this.index + 1;
            this.tokenEnd = this.xml.indexOf("'", this.tokenStart) - 1;

            if (this.tokenEnd !== -1) {
                this.body += '<encoding singleQuotes="true">' + this._token + "</encoding>";
                this.index = this.tokenEnd + 1;
            } else {
                throw this._getMessage("Illegal XML declaration encoding value.");
            }

            break;

        case '"':
            this.modeStack.pop();
            this.tokenStart = this.index + 1;
            this.tokenEnd = this.xml.indexOf('"', this.tokenStart) - 1;

            if (this.tokenEnd !== -1) {
                this.body += '<encoding>' + this._token + "</encoding>";
                this.index = this.tokenEnd + 1;
            } else {
                throw this._getMessage("Illegal XML declaration encoding value.");
            }

            break;


        default:
            throw this._getMessage("Illegal XML declaration encoding value.");
        }
    }

    /**
     * MODE_XML_DECLARATION_STANDALONE: We just detected an xml
     * declaration "standalone=" attribute and are now looking for the
     * value attribute in quotes.
     *
     * @link https://www.w3.org/TR/xml/#NT-XMLDecl
     */
    _modeXmlDeclarationStandalone() {
        if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space after 'standalone=' in XML declaration."));
            return;
        }

        if (this.xml.slice(this.index, this.index + 5) === '"yes"' || this.xml.slice(this.index, this.index + 5) === "'yes'") {
            LOGGER.silly(this._getMessage("Detected legal XML declaration standalone='yes' value."));
            this.index += 4;
            this.modeStack.pop();
            this.body += '<standalone>yes</standalone>';
        } else  if (this.xml.slice(this.index, this.index + 4) === '"no"' || this.xml.slice(this.index, this.index + 4) === "'no'") {
            LOGGER.silly(this._getMessage("Detected legal XML declaration standalone='no' value."));
            this.index += 3;
            this.modeStack.pop();
            this.body += '<standalone>no</standalone>';
        } else {
            throw this._getMessage("Illegal XML declaration standalone value.");
        }
    }

    /**
     * MODE_PROCESSING_INSTRUCTION_DATA: We have detected '<?PITARGET '
     * and are now scanning up to the next '?>'.
     *
     * @link https://www.w3.org/TR/xml/#NT-PI
     */
    _modeProcessingInstructionData() {
        if (this.currentChar === "?") {
            if (this.xml.slice(this.index, this.index + 2) === "?>") {
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.body += "<data>" + this._token + "</data></processingInstruction>";
                this.index++;
            }
        } else {
            LOGGER.silly(this._getMessage("Assuming a legal processing instruction data character."));
        }
    }

    /**
     * MODE_COMMENT: We have detected '<!--' and are now scanning up
     * to the next '-->'.
     *
     * @link https://www.w3.org/TR/xml/#NT-Comment
     */
    _modeComment() {
        if (this.currentChar === "-" &&
            this._getChar(this.index + 1) === "-" &&
            this._getChar(this.index + 2) === ">"
           ) {
            LOGGER.silly(this._getMessage("Detected the end of a comment."));
            this.modeStack.pop();
            this.tokenEnd = this.index - 1;
            this.body += "<comment>" + this._token + "</comment>";
            this.index += 2;
        } else {
            LOGGER.silly(this._getMessage("Assuming a legal comment character."));
            // TODO disallow illegal -- within a comment.
        }
    }

    /**
     * MODE_CDATA: We have detected '<![CDATA[' and are now scanning up
     * to the next ']]>'.
     *
     * @link https://www.w3.org/TR/xml/#NT-CDSect
     */
    _modeCdata() {
        if (this.currentChar === "]" &&
            this._getChar(this.index + 1) === "]" &&
            this._getChar(this.index + 2) === ">"
           ) {
            LOGGER.silly(this._getMessage("Detected the end of a cdata."));
            this.modeStack.pop();
            this.tokenEnd = this.index - 1;
            this.body += "<cdata>" + this._token + "</cdata>";
            this.index += 2;
        } else {
            LOGGER.silly(this._getMessage("Assuming a legal cdata character."));
            // TODO dissallow illegal character
        }
    }

    /**
     * MODE_ATTRIBUTE: We have detected a white space character
     * following a element tag name, so we are now looking for
     * attributes or the end of the element tag.
     */
    _modeAttribute() {
        switch (this.currentChar) {
        case ">":
            LOGGER.silly(this._getMessage("Detected end of attributes in element start tag."));
            this.modeStack.pop();
            this._modeElementStartOrEmptyTag();
            break;

        case "/":
            if (this._getChar(this.index + 1) === ">") {
                LOGGER.silly(this._getMessage("Detected end of attributes in element empty tag."));
                this.modeStack.pop();
                this._modeElementStartOrEmptyTag();
            } else {
                throw this._getMessage("Illegal '/' not followed by a '>' when scanning for attributes in element tag.");
            }
            break;

        default:
            if (this._isNameStartChar(this.index)) {
                this.modeStack.push(MODE_ATTRIBUTE_PREFIX_OR_NAME);
                this.tokenStart = this.index;
            } else if (this._isWhiteSpaceChar(this.index)) {
                LOGGER.silly(this._getMessage("Detected legal white space inside element tag after name."));
            } else {
                throw this._getMessage("Illegal character inside element tag after name.");
            }
        }
    }

    /**
     * ATTRIBUTE_PREFIX_OR_NAME: We are reading an attribute prefix or
     * name.
     */
    _modeAttributePrefixOrName() {
        if (this.currentChar === ":") {
            LOGGER.silly(this._getMessage("Detected attribute prefix-name separator character ':'."));
            this.tokenEnd = this.index - 1;
            this.attributePrefix = this._token;
            this.tokenStart = this.index + 1;
        } else if (this.currentChar === "=" || this._isWhiteSpaceChar(this.index)) {
            this.tokenEnd = this.index - 1;
            this.attributeName = this._token;
            LOGGER.silly(this._getMessage("Detected end of attribute name='" + this.attributeName + "'."));
            this.modeStack.pop();
            this.modeStack.push(MODE_ATTRIBUTE_EQUALS);
            this._modeAttributeEquals();
        } else {
            LOGGER.silly(this._getMessage("Assuming a legal attribute prefix or name character."));
            // TODO validate legal attribute prefix or name character
        }
    }

    /**
     * ATTRIBUTE_EQUALS: We have detected the end of an attribute name
     * and are now scanning for an '=' character.
     */
    _modeAttributeEquals() {
        if (this.currentChar === "=") {
            LOGGER.silly(this._getMessage("Detected attribute '=' character."));
            this.modeStack.pop();
            this.modeStack.push(MODE_ATTRIBUTE_VALUE);
        } else if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal whitespace while scanning for attribute '=' character."));
        } else {
            throw this._getMessage("Detected illegal character while scanning for attribute '=' character.");
        }
    }

    /**
     * ATTRIBUTE_VALUE: We have detected the '=' character before an
     * attribute value and are now scanning for single and double
     * quote characters which delimit the attribute value.
     */
    _modeAttributeValue() {
        switch (this.currentChar) {
        case "'":
            if (this.attributeValueQuotes === ATTRIBUTE_VALUE_QUOTES_UNDEFINED) {
                LOGGER.silly(this._getMessage("Detected start of single quoted attribute value."));
                this.attributeValueQuotes = ATTRIBUTE_VALUE_QUOTES_SINGLE;
                this.tokenStart = this.index + 1;
            } else {
                LOGGER.silly(this._getMessage("Detected end of single quoted attribute value."));
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.attributeValue = this._token;
                this._updateElementAttributes();
            }
            break;

        case '"':
            if (this.attributeValueQuotes === ATTRIBUTE_VALUE_QUOTES_UNDEFINED) {
                LOGGER.silly(this._getMessage("Detected start of double quoted attribute value."));
                this.attributeValueQuotes = ATTRIBUTE_VALUE_QUOTES_DOUBLE;
                this.tokenStart = this.index + 1;
            } else {
                LOGGER.silly(this._getMessage("Detected end of double quoted attribute value."));
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.attributeValue = this._token;
                this._updateElementAttributes();
            }
            break;

            // TODO case of escaped single or double quote

        default:
            LOGGER.silly(this._getMessage("Assuming a legal attribute value character or legal whitespace before attribute value."));
            // TODO validate legal attribute value character
        }
    }

    /**
     * MODE_ELEMENT_START_OR_EMPTY_TAG: We have just detected '<X'
     * where X is a NameStart character.
     */
    _modeElementStartOrEmptyTag() {
        switch (this.currentChar) {
        case ":":
            LOGGER.silly(this._getMessage("Detected an element tag prefix delimiter."));
            this.tokenEnd = this.index - 1;
            this.elementPrefix = this._token;
            this.tokenStart = this.index + 1;
            break;

        case ">":
            LOGGER.silly(this._getMessage("Detected end of an element start tag."));
            this.modeStack.pop();
            this.modeStack.push(MODE_ELEMENT_CONTENT);
            if (this.elementName === null) {
                this.tokenEnd = this.index - 1;
                this.elementName = this._token;
            }

            this.body += "<element><startTag>";
            this.body += this.elementPrefix ? "<prefix>" + this.elementPrefix + "</prefix>" : "";
            this.body += "<name>" + this.elementName + "</name>" + this.elementAttributes + "</startTag>";
            this._resetElementCache();
            break;

        case "/":
            if (this._getChar(this.index + 1) === ">") {
                LOGGER.silly(this._getMessage("Detected end of element empty tag."));
                this.modeStack.pop();

                if (this.elementName === null) {
                    this.tokenEnd = this.index - 1;
                    this.elementName = this._token;
                }

                this.body += "<element><emptyTag>";
                this.body += this.elementPrefix ? "<prefix>" + this.elementPrefix + "</prefix>" : "";
                this.body += "<name>" + this.elementName + "</name>" + this.elementAttributes + "</emptyTag></element>";
                this._resetElementCache();
                this.index++;
            } else {
                throw this._getMessage("Illegal '/' character in element tag prefix or name.");
            }
            break;

        default:
            if (this._isWhiteSpaceChar(this.index)) {
                this.modeStack.push(MODE_ATTRIBUTE);
                this.tokenEnd = this.index - 1;
                this.elementName = this._token;
            } else {
                LOGGER.silly(this._getMessage("Assuming a legal element tag prefix or name character."));
                // TODO check that this is a legal element tag character
            }
        }
    }

    /**
     * MODE_ELEMENT_CONTENT: We have just detected the end of an
     * element startTag. So we are now looking for:
     *
     * - Element endTag
     * - Element startTag (child element)
     * - Reference
     * - Cdata section
     * - Processing Instruction
     * - Comment
     * - Character data
     *
     * @see https://www.w3.org/TR/xml/#NT-content
     */
    _modeElementContent() {
        if (this.currentChar === "<") {
            LOGGER.silly(this._getMessage("Detected '<' character."));

            if (this._getChar(this.index + 1) === "/") {
                this.modeStack.pop();
                this.modeStack.push(MODE_ELEMENT_END_TAG);
                this.index++;
                this.body += "<endTag>";
                this.tokenStart = this.index + 1;
            } else if (this._isNameStartChar(this.index + 1)) {
                LOGGER.silly(this._getMessage("Detected start of an element."));
                this.modeStack.push(MODE_ELEMENT_START_OR_EMPTY_TAG);
                this.tokenStart = this.index + 1;
            } else if (this.xml.slice(this.index + 1, this.index + 9) === "![CDATA[") {
                LOGGER.silly(this._getMessage("Detected start of a cdata."));
                this.modeStack.push(MODE_CDATA);
                this.index += 9;
                this.tokenStart = this.index;
            } else if (this._getChar(this.index + 1) === "?") {
                LOGGER.silly(this._getMessage("Detected start of an XML Declaration or processing instruction."));
                this.modeStack.push(MODE_XML_DECLARATION_OR_PROCESSING_INSTRUCTION);
                this.index++;
                this.tokenStart = this.index + 1;
                // TODO set flag to say we only want an processing instruction!
            } else if (this.xml.slice(this.index + 1, this.index + 4) === "!--") {
                LOGGER.silly(this._getMessage("Detected start of a comment."));
                this.modeStack.push(MODE_COMMENT);
                this.index += 4;
                this.tokenStart = this.index;
            } else if (this._isNameStartChar(this.index + 1)) {
                LOGGER.silly(this._getMessage("Detected start of an element."));
                this.modeStack.push(MODE_ELEMENT_START_OR_EMPTY_TAG);
                this.tokenStart = this.index + 1;
            } else {
                throw this._getMessage("Detected illegal character in element content following a '<' character.");
            }
        } else if (this.currentChar === "&") {
            LOGGER.silly(this._getMessage("Detected start of a reference."));
            this.modeStack.push(MODE_TEXT);
            this.body += "<text>";
            this.modeStack.push(MODE_REFERENCE);
            this.tokenStart = this.index + 1;
        } else if (this._isChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected start of a text."));
            this.modeStack.push(MODE_TEXT);
            this.body += "<text>";
            this.tokenStart = this.index;
        } else {
            throw this._getMessage("Detected illegal character in element content.");
        }
    }

    /**
     * MODE_ELEMENT_END_TAG: We are in an element end tag and are
     * munching the prefix:name string looking for closing '>'
     * character.
     */
    _modeElementEndTag() {
        switch (this.currentChar) {
        case ">":
            this.modeStack.pop();
            this.tokenEnd = this.index - 1;
            this.body += "<name>" + this._token + "</name></endTag></element>";
            break;

        case ":":
            this.tokenEnd = this.index - 1;
            this.body += "<prefix>" + this._token + "</prefix>";
            this.tokenStart = this.index + 1;
            break;

        default:
            /* Munching element end tag prefix or name */
            /* TODO check legal name character. */
        }
    }

    /**
     * MODE_DOCTYPE: We have just detected a <!DOCTYPE so we are now expecting:
     *
     * - White space before the doctype name
     * - The first character of a doctype name
     *
     * @see https://www.w3.org/TR/xml/#NT-doctypedecl
     */
    _modeDoctype() {
        if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space while scanning for doctype name."));
        } else if (this._isNameStartChar(this.index)) {
            this.modeStack.pop();
            this.modeStack.push(MODE_DOCTYPE_NAME);
            this.tokenStart = this.index;
        } else {
            throw this._getMessage("Illegal character detected while scanning for doctype name.");
        }
    }

    /**
     * MODE_DOCTYPE_NAME: We have just detected the first character of
     * a doctype name. So we are now scanning the name looking for:
     *
     * - Legal name characters (continue reading)
     * - White space indicating the end of the name
     * - A '>' character indicating the end of the doctype
     * - A '[' indicating the start of an internal subset
     *
     * @see https://www.w3.org/TR/xml/#NT-doctypedecl
     */
    _modeDoctypeName() {
        if (this._isNameChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal name character while reading doctype name."));
        } else  if (this._isWhiteSpaceChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal white space and end of doctype name."));
            this.modeStack.pop();
            this.modeStack.push(MODE_DOCTYPE_CONTENT);
            this.publicExternalEntity = false;
            this.tokenEnd = this.index - 1;
            this.body += "<name>" + this._token + "</name>";
            this.doctypeNameSet = true;
        } else if (this.currentChar === ">") {
            this.modeStack.pop();
            if (this.doctypeNameSet) {
                this.body += "</documentTypeDeclaration>";
            } else {
                this.tokenEnd = this.index - 1;
                this.body += "<name>" + this._token + "</name></documentTypeDeclaration>";
            }
        } else if (this.currentChar === "[") {
            LOGGER.silly(this._getMessage("Detected start of internal subset when reading doctype name."));
            this.modeStack.pop();
            this.modeStack.push(MODE_DOCTYPE_CONTENT);
            this.modeStack.push(MODE_DOCTYPE_INTERNAL_SUBSET);
            this.tokenEnd = this.index - 1;
            this.body += "<name>" + this._token + "</name>";
            this.doctypeNameSet = true;
        } else {
            throw this._getMessage("Illegal character detected while scanning for doctype name.");
        }
    }

    /**
     * MODE_DOCTYPE_CONTENT: We are inside a doctype declaration and
     * have just read the doctype name followed by a space character.
     * So now we are looking for:
     *
     * - ExternalID
     * - Internal subset
     * - A '>' character which marks the end of the doctype declaration
     * - Whitespace
     *
     * @see https://www.w3.org/TR/xml/#NT-doctypedecl
     */
    _modeDoctypeContent() {
        switch (this.currentChar) {
        case 'S':
            if (this.xml.slice(this.index, this.index + 6) === "SYSTEM") {
                LOGGER.silly(this._getMessage("Detected start of SYSTEM literal in declaration version."));
                this.modeStack.push(MODE_DOCTYPE_SYSTEM_LITERAL);
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_UNDEFINED;
                this.index += 5;
            } else {
                throw this._getMessage("Illegal character in doctype declaration, read 'S' expecting 'SYSTEM'.");
            }
            break;

        case 'P':
            if (this.xml.slice(this.index, this.index + 6) === "PUBLIC") {
                LOGGER.silly(this._getMessage("Detected start of PUBLIC literal in declaration version."));
                this.modeStack.push(MODE_DOCTYPE_PUBLIC_LITERAL);
                this.body += "<publicExternalEntity>";
                this.publicExternalEntity = true;
                this.index += 5;
            } else {
                throw this._getMessage("Illegal character in doctype declaration, read 'P' expecting 'PUBLIC'.");
            }
            break;

        case '[':
                LOGGER.silly(this._getMessage("Detected start of internal subset in declaration version."));
                this.modeStack.push(MODE_DOCTYPE_INTERNAL_SUBSET);
            break;

        case '>':
            this.modeStack.pop();
            if (this.publicExternalEntity) {
                this.body += "</publicExternalEntity></documentTypeDeclaration>";
            } else {
                this.body += "</documentTypeDeclaration>";
            }
            break;

        default:
            if (this._isWhiteSpaceChar(this.index)) {
                LOGGER.silly(this._getMessage("Detected legal whitespace character in doctype content."));
            } else {
                throw this._getMessage("Illegal character detected while scanning doctype content. body=" + this.body);
            }
        }
    }

    /**
     * MODE_DOCTYPE_SYSTEM_LITERAL: We have just detected SYSTEM
     * inside a doctype declaration. We are expecting:
     *
     * - Whitespace before the system literal
     * - The system literal
     *
     * @see https://www.w3.org/TR/xml/#NT-SystemLiteral
     */
    _modeDoctypeSystemLiteral() {
        switch (this.currentChar) {
        case '"':
            if (this.literalQuotes === ATTRIBUTE_VALUE_QUOTES_UNDEFINED) {
                LOGGER.silly(this._getMessage("Detected start of system literal double quotes."));
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_DOUBLE;
                this.tokenStart = this.index + 1;
            } else {
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.body += "<systemLiteral>" + this._token + "</systemLiteral>";
            }
            break;

        case "'":
            if (this.literalQuotes === ATTRIBUTE_VALUE_QUOTES_UNDEFINED) {
                LOGGER.silly(this._getMessage("Detected start of system literal single quotes."));
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_SINGLE;
                this.tokenStart = this.index + 1;
            } else {
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.body += "<systemLiteral>" + this._token + "</systemLiteral>";
            }
            break;

        default:
            LOGGER.silly(this._getMessage("Assuming legal whitespace before or legal system literal character."));
        }
    }

    /**
     * MODE_DOCTYPE_PUBLIC_LITERAL: We have just detected PUBLIC
     * inside a doctype declaration. We are expecting:
     *
     * - Whitespace
     * - A public literal followed by whitespace followed by a system literal
     *
     * @see https://www.w3.org/TR/xml/#NT-ExternalID
     */
    _modeDoctypePublicLiteral() {
        switch (this.currentChar) {
        case '"':
            if (this.literalQuotes === ATTRIBUTE_VALUE_QUOTES_UNDEFINED) {
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_DOUBLE;
                this.tokenStart = this.index + 1;
            } else {
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.body += "<pubIdLiteral>" + this._token + "</pubIdLiteral>";
                this.modeStack.push(MODE_DOCTYPE_SYSTEM_LITERAL);
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_UNDEFINED;
            }
            break;

        case "'":
            if (this.literalQuotes === ATTRIBUTE_VALUE_QUOTES_UNDEFINED) {
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_SINGLE;
                this.tokenStart = this.index + 1;
            } else {
                this.modeStack.pop();
                this.tokenEnd = this.index - 1;
                this.body += "<pubIdLiteral>" + this._token + "</pubIdLiteral>";
                this.modeStack.push(MODE_DOCTYPE_SYSTEM_LITERAL);
                this.literalQuotes = ATTRIBUTE_VALUE_QUOTES_UNDEFINED;
            }
            break;

        default:
            LOGGER.silly(this._getMessage("Assuming legal whitespace before or legal public literal character."));
        }
    }

    /**
     * MODE_REFERENCE: We have just detected a '&' character. So we
     * are now reading legal entity reference name characters until a
     * closing ';' character.
     *
     * @see https://www.w3.org/TR/xml/#NT-Reference
     */
    _modeReference() {
        if (this.currentChar === "#") {
            LOGGER.silly(this._getMessage("Detected the start of a character reference."));
            this.modeStack.pop();
            this.modeStack.push(MODE_CHARACTER_REFERENCE);
            this.tokenStart = this.index + 1;
        } else if (this.currentChar === ';') {
            LOGGER.silly(this._getMessage("Detected the end of a reference."));
            this.modeStack.pop();
            this.tokenEnd = this.index - 1;
            this.body += '<entity title="&' + this._token + ';">' + this._token + "</entity>";
            this.tokenStart = this.index + 1;
        } else if (this._isNameChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal reference name character."));
        } else {
            throw this._getMessage("Detected illegal reference name character.");
        }
    }

    /**
     * MODE_CHARACTER_REFERENCE: We have just detected a '&#' character sequence. So we
     * are now reading legal character entity reference characters until a
     * closing ';' character.
     *
     * @see https://www.w3.org/TR/xml/#NT-CharRef
     */
    _modeCharacterReference() {
        switch (this.currentChar) {
        case "x":
            if (this.tokenStart === this.index ) {
                LOGGER.silly(this._getMessage("Detected a hexidecimal character reference."));
                this.hexidecimalCharacterReferenceSet = true;
            } else {
                throw this._getMessage("Detected 'x' as non-first character in character reference.");
            }
            break;

        case ";":
            LOGGER.silly(this._getMessage("Detected the end of a character reference."));
            this.modeStack.pop();
            this.tokenEnd = this.index - 1;
            this.body += '<entity title="&#' + this._token + ';">#' + this._token + "</entity>";
            this.tokenStart = this.index + 1;
            break;

        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
            LOGGER.silly(this._getMessage("Detected a legal digit in character reference."));
            break;

        case "a":
        case "b":
        case "c":
        case "d":
        case "e":
        case "f":
        case "A":
        case "B":
        case "C":
        case "D":
        case "E":
        case "F":
            if (this.hexidecimalCharacterReferenceSet) {
                LOGGER.silly(this._getMessage("Detected a legal hexidecimal character."));
            } else {
                throw this._getMessage("Detected illegal hexidecimal character in character reference.");
            }
            break

        default:
            throw this._getMessage("Detected illegal character in character reference.");
        }
    }

    /**
     * MODE_Text: We are reading legal text characters up until some control character.
     *
     * @see https://www.w3.org/TR/xml/#NT-Reference
     */
    _modeText() {
        if (this.currentChar === '<') {
            LOGGER.silly(this._getMessage("Detected end of text."));
            this.modeStack.pop();
            this.tokenEnd = this.index - 1;
            this.body += this._token + "</text>";
            this.index--;
        } else if (this.currentChar === '&') {
            LOGGER.silly(this._getMessage("Detected start of a reference."));
            this.modeStack.push(MODE_REFERENCE);
            this.tokenEnd = this.index - 1;
            this.body += this._token;
            this.tokenStart = this.index + 1;
        } else if (this._isChar(this.index)) {
            LOGGER.silly(this._getMessage("Detected legal text character."));
        } else {
            throw this._getMessage("Illegal character in text.");
        }
    }
}

/**
 * Export the getTranslator() function which returns a new instance of
 * the Translator class.
 */
exports.getTranslator = function() {
    return new Translator();
};

/**
 * Export the getLogger() function which returns the LOGGER.
 */
exports.getLogger = function() {
    return LOGGER;
};
